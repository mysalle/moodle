# syntax=docker/dockerfile:1
ARG PHP_VERSION=7.3
FROM moodlehq/moodle-php-apache:${PHP_VERSION}

ARG MOODLE_VERSION=v3.11.3
RUN git clone \
    --depth 1 \
    --branch ${MOODLE_VERSION} \
    https://github.com/moodle/moodle.git \
 && chown -R www-data:www-data moodle/
COPY --chown=www-data:www-data config.php /var/www/html/moodle/config.php

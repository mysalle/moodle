readyMariaDB:
	if ! docker-compose ps db ; then docker-compose up db ; fi
	./waith-mariadb.sh

.PHONY: readyMariaDB

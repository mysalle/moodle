#!/bin/bash

docker-compose logs db
until docker-compose logs db | grep -q 'mysqld: ready for connections.';
do
  echo 'Waiting for MariaDB to come up...'
  sleep 15
done

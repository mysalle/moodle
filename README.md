# Moodle

Estudi de com funciona per poder extreure les dades amb un elasticsearch.

## MariaDB
```bash
mariadb --user=moodle --password=m@0dl3ing moodle
```

```mariaDB
SELECT * FROM mdl_user;
SELECT * FROM mdl_course;
SELECT * FROM mdl_logstore_standard_log;
```

### Filant més prim amb `mdl_user`
```mariaDB
SELECT id, username, firstname, lastname, email, timecreated
FROM mdl_user;
```

### Filant més prim amb `mdl_course`
```mariaDB
SELECT id, fullname, shortname, timecreated
FROM mdl_course;
```

### Filant més prim amb `mdl_logstore_standard_log`
```mariaDB
SELECT id, eventname, component, action, target, userid, courseid, other, timecreated
FROM mdl_logstore_standard_log;
```

### Podem simplificar-ho amb una sola taula que contingui tota la informació:
```mariaDB
SELECT
  mdl_logstore_standard_log.id as id,
  mdl_logstore_standard_log.eventname,
  mdl_logstore_standard_log.component,
  mdl_logstore_standard_log.action,
  mdl_logstore_standard_log.target,
  mdl_logstore_standard_log.userid,
  mdl_user.username,
  mdl_user.firstname,
  mdl_user.lastname,
  mdl_user.email,
  mdl_logstore_standard_log.courseid,
  mdl_course.fullname,
  mdl_course.shortname,
  mdl_logstore_standard_log.other,
  mdl_logstore_standard_log.timecreated
FROM mdl_logstore_standard_log
LEFT JOIN (mdl_user, mdl_course) ON (
  mdl_logstore_standard_log.userid = mdl_user.id AND
  mdl_logstore_standard_log.courseid = mdl_course.id
);
```
